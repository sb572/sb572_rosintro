#!/usr/bin/bash

rosservice call /kill 'turtle1'

rosservice call /spawn 5 5 0 'turtle1'

rosservice call /spawn 7 5 0 'turtle2'

rosservice call turtle1/set_pen 50 5 50 5 0

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist\
        -- '[0.0,-2.0,0.0]' '[0.0,0.0,4.5]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist\
        -- '[0.0,-2.0,0.0]' '[0.0,0.0,-4.5]'

rosservice call turtle2/set_pen 5 50 50 5 0

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist\
	-- '[0.0,1.0,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist\
	-- '[0.0,1.5,0.0]' '[0.0,0.0,-4]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist\
	-- '[-1.0,-1.5,0.0]' '[0.0,0.0,-4.2]'


