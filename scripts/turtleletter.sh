#!/usr/bin/bash

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist\
	-- '[0.0,-2.0,0.0]' '[0.0,0.0,4.5]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist\
        -- '[0.0,-2.0,0.0]' '[0.0,0.0,-4.5]'
