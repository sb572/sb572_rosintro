
# importing important libraries
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander #important for MoveIt connection
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  
    from math import pi, fabs, cos, sqrt
    tau = 2.0 * pi #important if using Cartesian angles (I don't use but helpful for future changes)
    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

def all_close(goal, actual, tolerance): #tests if two values are within tolerance of eachother 
#specifically used in the go_to_pose_goal function to test the tolerance 
    
    if type(goal) is list:
        for index in range(len(goal)):
            if abs(actual[index] - goal[index]) > tolerance:
                return False

    elif type(goal) is geometry_msgs.msg.PoseStamped:
        return all_close(goal.pose, actual.pose, tolerance)

    elif type(goal) is geometry_msgs.msg.Pose:
        x0, y0, z0, qx0, qy0, qz0, qw0 = pose_to_list(actual)
        x1, y1, z1, qx1, qy1, qz1, qw1 = pose_to_list(goal)
        d = dist((x1, y1, z1), (x0, y0, z0))
        cos_phi_half = fabs(qx0 * qx1 + qy0 * qy1 + qz0 * qz1 + qw0 * qw1)
        return d <= tolerance and cos_phi_half >= cos(tolerance / 2.0)

    return True


class MoveGroupPythonInterfaceTutorial(object):
    

    def __init__(self): #sets up the class
        super(MoveGroupPythonInterfaceTutorial, self).__init__()
        moveit_commander.roscpp_initialize(sys.argv)
        #initialize node
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)
        robot = moveit_commander.RobotCommander()
        scene = moveit_commander.PlanningSceneInterface()
        group_name = "manipulator" #imporant to be named maniupulator as that is what the robot is
        move_group = moveit_commander.MoveGroupCommander(group_name)
        display_trajectory_publisher = rospy.Publisher( #display trajectory in RVIZ
        
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )
        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)
        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)
        group_names = robot.get_group_names()
        print("============ Available Planning Groups:", robot.get_group_names())
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")
        self.box_name = "" #variables
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names


    def go_to_pose_goal(self, xval, yval, zval): #function used to move to specific coordinates 
     
        move_group = self.move_group

        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.w = 1.0
        pose_goal.position.x = xval #by using xval, etc, it allows me to use as a function multiple times
        pose_goal.position.y = yval
        pose_goal.position.z = zval

        move_group.set_pose_target(pose_goal)

        success = move_group.go(wait=True)
   
        move_group.stop()
   
        move_group.clear_pose_targets()

        current_pose = self.move_group.get_current_pose().pose
        return all_close(pose_goal, current_pose, 0.01) #moves the robot




def main(): #function for what runs if main is called 
    try:

        input("Press Enter to begin drawing the Initals")
        tutorial = MoveGroupPythonInterfaceTutorial()

        input("Press Enter to draw the S") #draws S
        tutorial.go_to_pose_goal(0.2,.3,.6)
        tutorial.go_to_pose_goal(0.2,.4,.6)
        tutorial.go_to_pose_goal(0.2,.4,.55)
        tutorial.go_to_pose_goal(0.2,.3,.55)
        tutorial.go_to_pose_goal(0.2,.3,.5)
        tutorial.go_to_pose_goal(0.2,.4,.5)

        input("Press Enter to draw the L") #draws L
        tutorial.go_to_pose_goal(0.2,.4,.6)
        tutorial.go_to_pose_goal(0.2,.4,.5)
        tutorial.go_to_pose_goal(0.2,.3,.5)

        input("Press Enter to draw the B") #draws B
        tutorial.go_to_pose_goal(0.2,.4,.5)
        tutorial.go_to_pose_goal(0.2,.4,.55)
        tutorial.go_to_pose_goal(0.2,.4,.6)
        tutorial.go_to_pose_goal(0.2,.3,.6)
        tutorial.go_to_pose_goal(0.2,.3,.55)
        tutorial.go_to_pose_goal(0.2,.4,.55)
        tutorial.go_to_pose_goal(0.2,.3,.55)
        tutorial.go_to_pose_goal(0.2,.3,.5)
        tutorial.go_to_pose_goal(0.2,.4,.5)
        print("You have drawn the Intials S L B")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__": #runs the code (without it it would not run)
    main()

